import logging
import templateDataManager
from httpRequestHandlers import BaseRequestHandler

class HelpPageHandler(BaseRequestHandler):
    def get(self):
        logging.debug("In HelpPageHandler::get():\n%s", self.request)

        templateData = templateDataManager.getTemplateData(self.request)

        # Render the page
        self.render_response('helpPage.html', **templateData)

