applicationConfiguration = {
    'hostName':                         'localhost',
    'serverPort':                       8080,
    'helpEmailId':                      'itc-tech-week-2012@company-name.com',
    'registrationLink':                 'mailto:itc-tech-week-2012@company-name.com?Subject=Registration%20for%20codathon%202012',
    'salutationMarkup':                 '<b>India Tech Council</b> presents<br>a <b>Tech Fest 2012</b> event<br>',
    'contestStartTimeAsEpochTime':      1349463693,
    'contestDurationInMinutes':         180,
    'isDebugMode':                      False,
    'databasePath':                     '/home/arindam/workspace/webSide/codathon/rchhabra-rahul2-812d466bc7b0/codathon/database/Codathon.sqlite',
}
