import logging
from configurationManager import applicationConfiguration
from SQLiteUtil import SQLiteUtil

sqlUtil = SQLiteUtil()

templateData = {}
lastTeamCode = None

def initialize():
    global templateData

    logging.debug("In templateDataManager::initialize()...")

    # Set static elements
    templateData = {
        # Set the wrapperElements
        'wrapperElementStart':          '<div class="wrapper"> \
                                            <div id="topColorStrip">&nbsp;</div> \
                                            <div class="row"> \
                                                <div class="twocol"></div> \
                                                <div class="eightcol" style="text-align: center; color: #eee; font-size: 2.5em; padding-bottom: .2em; line-height: 1.3em;"> \
                                                    ' + applicationConfiguration['salutationMarkup'] + '\
                                                    <a style="font-weight: 700; color: #f60; font-size: 1.1em;">codathon</a> \
                                                </div> \
                                                <div class="twocol last"></div> \
                                            </div> \
                                            <div class="row"> \
                                                <div class="twocol"></div> \
                                                <div class="eightcol" style="padding-top: 1em; text-align: center; border-top: 2px dashed #f60;">',

        'wrapperElementEnd':            '       </div> \
                                                <div class="twocol last"></div> \
                                            </div> \
                                            <div class="push"></div> \
                                         </div>',

        # Set appropriate url for jQuery
        'jQueryScriptUrl':              '../js/jquery/1.7.2/jquery.min.js' if 'isDebugMode' not in applicationConfiguration or applicationConfiguration['isDebugMode'] is not False
                                        else 'https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js',

        'totalQuestions': sqlUtil.getTotalQuestions()
    }

    # Read applicationConfiguration
    templateData.update(applicationConfiguration)

    # Set time elements
    '''templateData['contestDurationInMinutes']  = int(applicationConfiguration['numberOfQuestions']) * int(applicationConfiguration['questionReleaseIntervalInMinutes'])
    templateData['contestEndTimeAsEpochTime'] = int(applicationConfiguration['contestStartTimeAsEpochTime']) + int(templateData['contestDurationInMinutes'] * 60)'''

    templateData['contestEndTimeAsEpochTime'] = int(applicationConfiguration['contestStartTimeAsEpochTime']) + int(applicationConfiguration['contestDurationInMinutes']*60)

    populateFooter('')

    logging.debug('Initial templateData:\n%s' % templateData)

def populateFooter(currentTeamCode):
    global templateData, lastTeamCode

    # Nnormalize the value of currentTeamCode
    if currentTeamCode is None:
        logging.debug('Received an empty teamCode in templateDataManager::populateFooter()')
        currentTeamCode = ''
    elif currentTeamCode is '' or sqlUtil.getTeamDetailsByTeamCode(currentTeamCode) is "":
        logging.critical('Received an unrecognized teamCode [%s] in templateDataManager::populateFooter()', currentTeamCode)
        currentTeamCode = ''
    else:
        logging.debug("In templateDataManager::populateFooter() for teamCode[%s]..." % currentTeamCode)

    # Don't bother with an update if the lastTeamCode has not changed
    if lastTeamCode is currentTeamCode:
        return
    lastTeamCode = currentTeamCode
    templateData['teamCode'] = currentTeamCode

    teamCodeUrlSuffix = '' if currentTeamCode is '' else '?teamCode=' + currentTeamCode

    # Set the footer element
    templateData['footerElement'] = \
    '<div class="footer row"> \
        <div class="twocol"></div> \
        <div class="eightcol" style="padding-top: 1.0em; border-top: 2px dashed #f60;"> \
            <div style="float: left; width: 20%;"><a href="/home'+ teamCodeUrlSuffix +'"><img src="../images/home.png" title="home"/></a></div> \
            <div style="float: left; width: 20%;"><a href="/questions'+ teamCodeUrlSuffix +'"><img src="../images/question.png" title="questions" /></a></div> \
            <div style="float: left; width: 20%;"><a href="/leaderboard'+ teamCodeUrlSuffix +'"><img src="../images/leaderboard.png" title="leaderboard"/></a></div> \
            <div style="float: left; width: 20%;"><a href="/announcements'+ teamCodeUrlSuffix +'"><img src="../images/announcement.png" title="announcements"/></a></div> \
            <div style="float: left; width: 20%;"><a href="/help'+ teamCodeUrlSuffix +'"><img src="../images/help.png" title="help & faq"/></a></div> \
            <script>$(".footer div img").hover(function(){$(this).animate({"padding-top": "-=10px"}, 200)}, function(){$(this).animate({"padding-top": "+=10px"}, 200)});</script> \
        </div> \
        <div class="twocol last"></div> \
    </div>'

def getTemplateData(request):
    global templateData
    logging.debug("In templateDataManager::getTemplateData()...")

    populateFooter(request.get('teamCode'))

    return templateData

