import logging
import templateDataManager
from httpRequestHandlers import BaseRequestHandler
from configurationManager import applicationConfiguration
from SQLiteUtil import SQLiteUtil
from Team import Team
import jinja2
from jinja2 import Environment
import time

sqlUtil = SQLiteUtil()

class LeaderboardPageHandler(BaseRequestHandler):
    def get(self):
        logging.debug("In HomePageHandler::get():\n%s", self.request)

        templateData = templateDataManager.getTemplateData(self.request)

        currentTimeAsEpochTime = int(time.mktime(time.localtime()))
        templateData['currentTimeAsEpochTime'] = currentTimeAsEpochTime
        logging.debug('currentTimeAsEpochTime: [%s] contestStartTimeAsEpochTime: [%s] contestEndTimeAsEpochTime: [%s]' % \
                        (currentTimeAsEpochTime, templateData['contestStartTimeAsEpochTime'], templateData['contestEndTimeAsEpochTime']))


        if currentTimeAsEpochTime < templateData['contestStartTimeAsEpochTime']:
            templateData['countDownBoxMarkUp'] = \
                '<b>codathon <span style="color: #4a4">starts</span></b> in : ' \
                '<b id="days">00</b> days <b id="hours">00</b> hours <b id="minutes">00</b> minutes <b id="seconds">00</b> seconds <script>startCountDown(%s)</script>' \
                % (templateData['contestStartTimeAsEpochTime'] - currentTimeAsEpochTime)
            '''Contest Not Started'''
        elif currentTimeAsEpochTime >= templateData['contestEndTimeAsEpochTime']:
            templateData['countDownBoxMarkUp'] = '<b>codathon has <span style="color: #c22">ended</span></b>'
            '''Contest Ended'''
        else:
            templateData['countDownBoxMarkUp'] = \
                '<b>codathon <span style="color: #c22">ends</span></b> in : ' \
                '<b id="hours">00</b> hours <b id="minutes">00</b> minutes <b id="seconds">00</b> seconds <script>startCountDown(%s)</script>' \
                % (templateData['contestEndTimeAsEpochTime'] - currentTimeAsEpochTime)
            '''Contest Ongoing'''

        leaderboardList = []
        leaderboardList = sqlUtil.getAllTeamDetails(onlyScorers=True)
        templateData['leaderboard'] = leaderboardList
        logging.debug('Current leaderboardList after data:\n%s' % leaderboardList)

        # Render the page
        self.render_response('leaderboardPage.html', **templateData)

