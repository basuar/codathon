import os
import ast
import errno
import inspect
import logging
import datetime
from config.applicationConfiguration import applicationConfiguration

def initialize():
    # Initialize logging configuration
    logging.basicConfig(filename = '%s/../logs/codathon.%s.log' % (os.path.dirname(inspect.getfile(inspect.currentframe())), datetime.datetime.now().date()),
                        format   = '%(asctime)s %(levelname)s: %(message)s',
                        level    = logging.DEBUG if applicationConfiguration['isDebugMode'] is True else logging.INFO)
