import time
from configurationManager import applicationConfiguration

class Team:
    """A simple Team class"""
    team_name = ""
    team_code = ""
    team_score = ""
    team_current_question = ""
    team_lastSuccessfulSubmissionTimeAsEpochTime = 0

    def __init__(self,team_name,team_code,team_score,team_current_question,team_lastSuccessfulSubmissionTimeAsEpochTime):
    	self.team_name = team_name
    	self.team_code = team_code
    	self.team_score = team_score
    	self.team_current_question = team_current_question
    	self.team_lastSuccessfulSubmissionTimeAsEpochTime = team_lastSuccessfulSubmissionTimeAsEpochTime

    def get_team_name(self):
        return self.team_name

    def get_team_code(self):
        return self.team_code

    def get_team_score(self):
        return self.team_score

    def get_team_current_question(self):
        return self.team_current_question

    def get_team_lastSuccessfulSubmissionTimeAsEpochTime(self):
        return self.team_lastSuccessfulSubmissionTimeAsEpochTime

    def get_formatted_total_time_taken(self):
        started_time = int(applicationConfiguration['contestStartTimeAsEpochTime'])
        actual_time_taken =  self.team_lastSuccessfulSubmissionTimeAsEpochTime - started_time
        if actual_time_taken > 0:
            return time.strftime('%Hh %Mm %Ss', time.gmtime(actual_time_taken))
        else:
            return '00h 00m 00s'
