import time
import logging
import templateDataManager
from httpRequestHandlers import BaseRequestHandler
from SQLiteUtil import SQLiteUtil

sqlUtil = SQLiteUtil()

class QuestionsPageHandler(BaseRequestHandler):
    def get(self):
        logging.debug("In QuestionsPageHandler::get()")

        templateData = templateDataManager.getTemplateData(self.request)

        logging.debug('full Request:\n%s' %self.request )

         # Render the page
        currentTimeAsEpochTime = int(time.mktime(time.localtime()))
        templateData['currentTimeAsEpochTime'] = currentTimeAsEpochTime
        logging.debug('currentTimeAsEpochTime: [%s] contestStartTimeAsEpochTime: [%s] contestEndTimeAsEpochTime: [%s]' % \
                        (currentTimeAsEpochTime, templateData['contestStartTimeAsEpochTime'], templateData['contestEndTimeAsEpochTime']))

        if currentTimeAsEpochTime < templateData['contestStartTimeAsEpochTime']:
            templateData['countDownBoxMarkUp'] = \
                '<b>codathon <span style="color: #4a4">starts</span></b> in : ' \
                '<b id="days">00</b> days <b id="hours">00</b> hours <b id="minutes">00</b> minutes <b id="seconds">00</b> seconds <script>startCountDown(%s)</script>' \
                % (templateData['contestStartTimeAsEpochTime'] - currentTimeAsEpochTime)
            '''Contest Not Started'''
        elif currentTimeAsEpochTime >= templateData['contestEndTimeAsEpochTime']:
            templateData['countDownBoxMarkUp'] = '<b>codathon has <span style="color: #c22">ended</span></b>'
            '''Contest Ended'''
        else:
            templateData['countDownBoxMarkUp'] = \
                '<b>codathon <span style="color: #c22">ends</span></b> in : ' \
                '<b id="hours">00</b> hours <b id="minutes">00</b> minutes <b id="seconds">00</b> seconds <script>startCountDown(%s)</script>' \
                % (templateData['contestEndTimeAsEpochTime'] - currentTimeAsEpochTime)
            '''Contest Ongoing'''

        if templateData['teamCode'] is not '':
            team = sqlUtil.getTeamDetailsByTeamCode(templateData['teamCode'])
            if team:
                templateData['currentTeamName'] = team.get_team_name()
                templateData['currentTeamPoints'] = team.get_team_score()
                templateData['currentTeamQuestionIndex'] = team.get_team_current_question()
                templateData['currentTeamTimeTaken'] = team.get_formatted_total_time_taken()

                if templateData['currentTeamQuestionIndex'] <= templateData['totalQuestions']:
                    question = sqlUtil.getQuestionByIdQuery(templateData['currentTeamQuestionIndex'])
                    if question:
                        templateData['currentQuestionMarkup'] = question.get_question()
                        templateData['currentQuestionTestCases'] = question.get_test_case_file_path()
                    else:
                        logging.critical('Count not fetch question with id[%s]' % templateData['currentTeamQuestionIndex'])

        self.render_response('questionsPage.html', **templateData)
