import logging

# Application's main function
def main():
    # Read application configuration
    import configurationManager
    from configurationManager import applicationConfiguration
    configurationManager.initialize()

    logging.info('Starting the [codathon] server on host [%s]:[%s]' % (applicationConfiguration['hostName'], applicationConfiguration['serverPort']))
    logging.info('Initial applicationConfiguration:\n%s' % applicationConfiguration)

    import templateDataManager
    templateDataManager.initialize()

    # Prepare HTTP request routers
    import webapp2
    app = webapp2.WSGIApplication([ ('/', 'indexPageHandler.IndexPageHandler'),
                                    ('/home', 'homePageHandler.HomePageHandler'),
                                    ('/questions', 'questionsPageHandler.QuestionsPageHandler'),
                                    ('/checkResult', 'checkResultHandler.CheckResultHandler'),
                                    ('/leaderboard', 'leaderboardPageHandler.LeaderboardPageHandler'),
                                    ('/announcements', 'announcementsPageHandler.AnnouncementsPageHandler'),
                                    ('/testcase', 'testcasePageHandler.TestcasePageHandler'),
                                    ('/help', 'helpPageHandler.HelpPageHandler'),
                                    (r'/.+[png|css|js]', 'httpRequestHandlers.StaticDataHandler'),
                                  ], debug = applicationConfiguration['isDebugMode'])

    # Run HTTP server.
    from paste import httpserver
    httpserver.serve(app, applicationConfiguration['hostName'], applicationConfiguration['serverPort'])

if __name__ == '__main__':
    main()
