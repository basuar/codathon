import time
import logging
import templateDataManager
from httpRequestHandlers import BaseRequestHandler
from configurationManager import applicationConfiguration
from SQLiteUtil import SQLiteUtil
import Team
import Question

class CheckResultHandler(BaseRequestHandler):
    sqlUtil = SQLiteUtil()
    def checkTeamCode(self):
        _userInputTeamCode = self.request.get('teamCode')

        logging.info('Checking for team %s' % _userInputTeamCode)
        team=sqlUtil.getTeamDetailsByTeamCode(_userInputTeamCode)

        result = False
        if team:
            logging.info('Team found for teamCode [%s]' % _userInputTeamCode)
            result = True
        else:
            logging.info('Team NOT found for teamCode\n%s' % _userInputTeamCode)

        self.response.out.write('OK' if (result) else 'NOT OK')

    def checkUserSolution(self, teamCode):
        result = False
        team = sqlUtil.getTeamDetailsByTeamCode(teamCode)
        questionId = self.request.get('questionId')
        answer = self.request.get('answer')
        logging.info('Received submission from teamCode [%s] for questionId [%s] with answer [%s]' % (teamCode, questionId, answer))
        if team and questionId and answer:
            if int(questionId) is team.get_team_current_question():
                if ''+answer == ''+sqlUtil.getAnswerByIdQuery(questionId)[0]:
                    logging.info('Correct answer from teamCode [%s] for questionId [%s]' % (teamCode, questionId))
                    sqlUtil.updateTeamPoints(teamCode, int(time.mktime(time.localtime())))
                    result = True
                else:
                    logging.info('Incorrect answer from teamCode [%s] for questionId [%s]' % (teamCode, questionId))
            else:
                logging.error('Received answer for incorrect questionId [%s] instead of [%s] from teamCode [%s]' % (questionId, team.get_team_current_question(), teamCode))
        else:
            logging.critical('Missing data for processing checkUserSolution() for teamCode [%s]' % teamCode) 
        self.response.out.write('OK' if (result) else 'NOT OK')

    def get(self):
        global sqlUtil
        sqlUtil = SQLiteUtil()
        _request = self.request.get('requestType')
        _userInputTeamCode = self.request.get('teamCode')

        logging.debug('Request type is %s' % _request)
        if _request == 'userValidation':
            self.checkTeamCode()
        elif _request == 'checkUserSolution':
            self.checkUserSolution(_userInputTeamCode)
        else:
            logging.critical("Incorrect ajax request for requestType [%s]" % _request)
            self.response.set_status(404)
